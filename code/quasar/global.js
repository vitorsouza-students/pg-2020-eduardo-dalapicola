const { fork } = require('child_process')
const path = require('path')

const node = process.argv[0]
const { dir } = path.parse(node)
const script = process.argv[2]
const splited = script.split(' ')
const exec = splited.shift()
const args = [...splited]

fork(`${dir}/${exec}`, args)
