export function getUser (state) {
  return state.user
}

export function isProfessor (state) {
  return state.user?.professor_id !== null
}

export function isSecretary (state) {
  return state.user?.secretary_id !== null
}

export function isAuthenticated (state) {
  return Boolean(state.user)
}

export function isChief (state, getters) {
  return getters.isProfessor && state.user.is_chief
}