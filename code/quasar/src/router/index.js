import Vue from 'vue'
import VueRouter from 'vue-router'

import LoginDialog from 'components/LoginDialog'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

let flag = false

export default function ({ store/*, ssrContext*/ }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    const user = store.getters['auth/getUser']

    if (!user) {
      if (to.matched.some(route => route.meta.authOnly)) {
        if (flag) return
        flag = true
        Vue.prototype.$q.notify({
          type: 'warning',
          message: 'Faça o login para acessar esta página.'
        })

        return Vue.prototype.$q.dialog({
          component: LoginDialog,
          storeRef: store,
          routerRef: Router,
          onLogin: user => {
            user && next(to)
          },
          onRegister: status => {
            status && next({ name: 'index' })
          },
          onForgotPassword: status => {
            status && next({ name: 'index' })
          },
          onCancel: () => {
            return next(from.name ? from : { name: 'index' })
          },
          onHide: () => {
            flag = false
          }
        })
      }
      return next()
    } else {
      if (to.matched.some(route => route.name === 'index')) {
        return next({ name: 'panel' })
      }
    }

    const isSecretary = store.getters['auth/isSecretary']
    const isProfessor = store.getters['auth/isProfessor']

    if (
      (isSecretary && to.matched.some(route => route.meta.professorOnly)) ||
      (isProfessor && to.matched.some(route => route.meta.secretaryOnly))) {
      Vue.prototype.$q.notify({
        type: 'warning',
        message: 'Você não possui permissão para acessar esta página.'
      })

      return next({ name: 'panel' })
    }

    return next()
  })

  return Router
}
