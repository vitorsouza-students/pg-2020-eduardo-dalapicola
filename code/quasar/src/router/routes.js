const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'index',
        path: '',
        component: () => import('pages/Index.vue'),
      },
    ],
  },
  {
    path: '/painel',
    component: () => import('layouts/PanelLayout.vue'),
    children: [
      {
        name: 'panel',
        path: '',
        component: () => import('pages/IndexPanel.vue'),
        meta: {
          authOnly: true
        }
      },
      {
        name: 'professor.list',
        path: 'professores',
        component: () => import('pages/User/ProfessorList.vue'),
        meta: {
          authOnly: true,
          secretaryOnly: true
        }
      },
      {
        name: 'secretary.list',
        path: 'secretarios',
        component: () => import('pages/User/SecretaryList.vue'),
        meta: {
          authOnly: true,
          secretaryOnly: true
        }
      },
      {
        name: 'withdrawal.list',
        path: 'afastamentos',
        component: () => import('pages/WithDrawal/WithDrawalList.vue'),
        meta: {
          authOnly: true,
          secretaryOnly: true
        }
      },
      {
        name: 'withdrawal.tab',
        path: 'afastamentos/tab',
        component: () => import('pages/WithDrawal/WithdrawalTab/Index.vue'),
        redirect: { name: 'withdrawal.tab.my' },
        meta: {
          authOnly: true,
          professorOnly: true
        },
        children: [
          {
            name: 'withdrawal.tab.my',
            path: 'meus-afastamentos',
            component: () => import('pages/WithDrawal/WithdrawalTab/MyWithdrawalList.vue'),
            meta: {
              authOnly: true,
              professorOnly: true
            },
          },
          {
            name: 'withdrawal.tab.rapporteur',
            path: 'relator',
            component: () => import('pages/WithDrawal/WithdrawalTab/RapporteurWithdrawalList.vue'),
            meta: {
              authOnly: true,
              professorOnly: true
            },
          },
          {
            name: 'withdrawal.tab.others',
            path: 'outros-afastamentos',
            component: () => import('pages/WithDrawal/WithdrawalTab/OtherWithdrawalList.vue'),
            meta: {
              authOnly: true,
              professorOnly: true
            },
          }
        ]
      },
      {
        name: 'feedback.list',
        path: 'pareceres',
        component: () => import('pages/Feedback/FeedbackList.vue'),
        meta: {
          authOnly: true,
          professorOnly: true
        }
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
