import axios from 'axios'
import { objToFormData } from 'src/utils/formData.js'
import { handleErrors } from 'src/utils/api'

export default ({ Vue, router, store }) => {
    Vue.prototype.$axios = axios.create({
        baseURL: process.env.API_URL,
        timeout: 180000,
        headers: {
            'accept': 'application/json'
        }
    })

    Vue.prototype.$axios.formDataPost = async function (url, data) {
        return await this.post(
            url
            , objToFormData(data)
            // , { headers: { 'Content-Type': 'multipart/form-data' } }
        )
    }

    Vue.prototype.$axios.formDataPut = async function (url, data) {
        const formData = objToFormData(data)
        // formData.append('_method', 'PUT')
        return await this.put(
            url
            , formData
            // , { headers: { 'Content-Type': 'multipart/form-data' } }
        )
    }

    Vue.prototype.$axios.interceptors.request.use(request => {
        const token = localStorage.getItem('access_token')
        if (token) request.headers.Authorization = `Bearer ${token}`
        if (!request.disableLoading) Vue.prototype.$q.loading.show()

        return request
    }, error => {
        Vue.prototype.$q.loading.hide()

        return Promise.reject(error)
    })

    Vue.prototype.$axios.interceptors.response.use(response => {
        Vue.prototype.$q.loading.hide()
        if (response.data?.message) {
            Vue.prototype.$q.notify({
                type: 'positive',
                message: response.data?.message
            })
        }
        return response
    }, error => {
        Vue.prototype.$q.loading.hide()

        if (!error.config.ignoreErrorHandling) {
            return handleErrors(error, { router, store, axios })
        }

        return Promise.reject(error)
    })
}
