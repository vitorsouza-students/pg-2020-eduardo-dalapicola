
function objToFormDataRecursive (content, formData, prefix) {
  if (content === null) {
    return
  }
  if (typeof (content) === 'object' && !(content instanceof Blob) && !(content instanceof Array)) {
    for (const [key, value] of Object.entries(content)) {
      const cprefix = prefix === '' ?
        key :
        prefix + "['" + key + "']"

      objToFormDataRecursive(value, formData, cprefix)
    }

  }
  else if (typeof (content) === 'object' && content instanceof Array) {
    for (const [i, value] of content.entries()) {
      objToFormDataRecursive(value, formData, prefix + `[${i}]`)
    }

  }
  else if (['string', 'number'].indexOf(typeof (content)) >= 0) {
    formData.append(prefix, content + '')
  }
  else if (typeof (content) === 'object' && content instanceof Blob) {
    formData.append(prefix, content)
  }
}

export function objToFormData (content) {
  if (typeof (content) !== 'object') {
    throw "data content must be an object"
  }
  const formData = new FormData()
  objToFormDataRecursive(content, formData, '')

  // Exibição dos valores chave/valor
  // for (var [k, v] of formData.entries()) {
  //   console.log(k + ', ' + v);
  // }

  return formData
}