import Vue from 'vue'
import { Notify } from 'quasar'
import LoginDialog from 'components/LoginDialog'

export const handleErrors = async (error, { router, store, axios }) => {
    if (!window.navigator?.onLine) {
        return Notify.create({
            type: 'warning',
            message: 'Sem conexão com a internet.'
        })
    }

    if (!error.response || typeof error.response !== 'object') {
        return Notify.create({
            type: 'warning',
            message: 'Problemas de conexão, por favor tente mais tarde.'
        })
    }

    const { status, data } = error.response

    const handleError = {
        401: async () => {
            await store.dispatch('auth/logout')

            // const errors = data.errors && Object.values(data.errors).flat()

            if (data[0]?.field === 'password' && data[0]?.message === 'Invalid user password') {
                return Vue.prototype.$q.notify({
                    type: 'warning',
                    message: data[0]?.message
                })
            }

            Vue.prototype.$q.notify({
                type: 'warning',
                message: 'Sua sessão expirou, por favor faça novamente o login.'
            })
            return new Promise((resolve, reject) => {
                Vue.prototype.$q.dialog({
                    component: LoginDialog,
                    storeRef: store,
                    routerRef: router,
                    onLogin: async (data) => {
                        if (!data) return

                        error.config.headers.Authorization = `Bearer ${data.token}`

                        const res = await axios(error.config)

                        resolve(res)
                    },
                    onCancel: () => {
                        router.push({ name: 'index' })
                        reject(error)
                    }
                })
            })

        },
        403: () => {
            router.replace({ name: 'index' })
            Vue.prototype.$q.notify({
                type: 'warning',
                message: data?.message
            })
        },
        404: () => {
            Vue.prototype.$q.notify({
                type: 'warning',
                message: data?.message
            })
        },
        422: () => {
            const message = data.message || Object.values(data.errors).flat().shift()
            Vue.prototype.$q.notify({
                type: 'warning',
                message
            })
        },
        429: () => {
            Vue.prototype.$q.notify({
                type: 'negative',
                message: 'Você está realizando muitas ações em um curto período de tempo, por favor tente novamente mais tarde.'
            })
        },
        500: () => {
            Vue.prototype.$q.notify({
                type: 'negative',
                message: 'Não foi possível realizar esta ação no momento. tente novamente mais tarde.'
            })
        }
    }

    return (handleError[status] && await handleError[status]()) || Promise.reject(error)
}
