
=> Apresentação:
Olá, boa tarde a todos,
meu nome é Eduardo Gorayeb Dalapicola e meu orientador é o professor Vítor Estêvão Silva Souza.
O tema do meu trabalho é a Aplicação do método Frameweb no sistema SCAP utilizando os frameworks AdonisJS e Quasar

=> Apresentar o sumário
Objetivo
Metodologia
Referencial Teórico
Diagrama de caso de uso
diagrama de classes
projeto arquitetural
resultado
conclusao

=> Referencial Teórico

No ínicio as aplicações eram feitas sem rigor técnico, com o tempo foi percebendo-se a necessidade de organização e foram criados os conceitos de engenharia de software que traz uma abordagem sistemática para a criação de softwares. A engenharia web é uma sub-área da engenharia de software especifica para sistemas web, foram criados várias abordagens e métodos para a criação de sistemas Web, uma delas é o método FrameWeb, um método proposto pelo meu orientador vítor, que faz a utilização de categorias de frameworks para guiar o desenvolvimento.


O método propõe a divisão do sistema em 3 partes: Logica de apresentação, que é responsável pela parte gráfica (HTML,CSS,JS, imagens) e interação com o usuário (controle). Lógica de negócio, que é responsável pela implementação da lógica extraída na análise de requisitos, implementação dos casos de uso. E a lógica de acesso a dados, que faz a interface com o banco de dados provendo dados para aplicação. O método frameweb recomenda o padrão DAO.

Para auxiliar na implementação das camadas são recomendados a produção de modelos para cada camada: Os modelos de entidades para representarem os conceitos do domínio do problema, Os modelos de persistência que representam a camada de acesso a dados, modelos de navegação para mostrar a dinâmica da camada de apresetação e os modelos de aplicação para representar as classes de serviço e as dependencias com outros componentes do sistema.


=> Caso de Uso
Agora iremos entrar especificamente na análise de requisitos do sistema SCAP: Nesse modelo de caso de está indicando todoso os usuários do sistema, Professor, Chefe de departamento e Secretário e como eles interagem com o sistema.
(Fazer a apresentação das ações dos usuários)

=> Diagrama de classes
Este é o diagrama de classes que mostra os conceitos do sistema e a relação entre eles:
(Mudar para a foto no computador e dar zoom e explicar o modelo)

=> Projeto arquitetural
Entrando agora na parte do prpojeto arquitetural, podemos observar neste modelo à esquerda uma linha tracejada, onde há a divisão dos dois sistemas implementados: A camada de apresentação que foi implementada pelo framework Quasar, que provê componentes gráficos em HTML, JS, CSS para interação com usuário e realiza requisições AJAX para o servidor REST do AdonisJS que processas as requisições e devolve em formato JSON os dados para serem visualizados na tela.

No adonis foram implementadas as seguintes camadas:
O cliente realiza um ação ou requisita um dado, esta requisição passa pela autenticação: onde é identificado qual cliente está tentando se comunicar com o servidor, pela autorização que é verificada a autorização que o cliente tem para acessar aquele módulo, depois esta requisição chega a camada de controle, onde os dados são extraídos e enviados ou para a camada de serviço caso seja um caso de uso e pode ser também enviado diretamente para a camada de repositório (Acesso a dados) quando é uma funcionalidade de apenas retornar dados e não tem lógica envolvida.
O repositório representa a camada de acesso a dados. esta ligado diretamente com O LucidJS (ORM - Mapeamento Objeto relacional onde estão os modelos do banco de dados) que faz interface direta com o banco de dados.

Ambos os frameworks são javascript.

=> Projeto Arquitetural
Agora serão mostrados os modelos do FrameWeb que foram construídos.

O modelo de entidades é parecido com o modelo de classes porém foi específicado para a linguagem de programação utilizada Javascript
ENUM => Pró-Reitoria de Pesquisa e Pós-Graduação, Centro Tecnológico, Departamento de Informática

No modelo de persistência é recomendado a utilização de interfaces, porém não foi utilizado pois javascript não implementa interface.
(explicar alguns métodos)

No modelo de navegação deveriam ser representados os atributos das classes de ação, porém a forma de construção do framework injeta os dados nos métodos, diferentemente de como é proposto pelo modelo original.
(explicar os modelos)
Em solicitar afastamento os retornos do método store podem ser uma mensagem de sucesso ou uma mensagem de errom, é assim para todas as ações de atualizar/criar do sistema.
Em cadastrar usuários o método listProfessors é utilizado para preencher o select de escolha de algum professor para ser parente.

O modelo de aplicação também é recomendado o uso de interfaces para a realização das dependencias entre as camadas, porém pelo mesmo motivo anterior não foi implementado. (explicar as dependencias)


=> Resultado:
Mostrar video

Logar com Professor1, Chefe do departamento e Secretário

Secretário:

1 - Logar com secretario@email.com
2 - mostrar as funcionalidades de criar secretários e falar sobre os filtros
3 - criar professor 3, criar com mandato e parente
4 - editar o professor1 e colocar como parente do professor2, mostrar que aparece nas duas listas
5 - mostrar a listagem e edição dos afastamentos e a troca de situação
6 - jogar um afastamento de aprovado-CT para aprovado-PRPPG com um documento.
7 - jogar um afastamento para reprovado

Professor 1
1 - Ir logado como professor
2 - mostrar "Meus Dados"
3 - Entrar em afastamentos e falar sobre emitir parecer e documentos
4 - Criar um afastamento internacional
5 - Emitir um parecer
6 - mostrar a listagem de pareceres


Chefe de Departamento
1 - mostrar como escolhe o relator
2 - escolher o professor3 como relator do pedido do professor 1
