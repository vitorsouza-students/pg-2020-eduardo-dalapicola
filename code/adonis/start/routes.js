'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {

  Route.post('login', 'AuthController.login')
  Route.post('signup', 'UserController.storeGuest')

})
  .prefix('api')
  .middleware('guest')

// AUTH ROUTES
Route.group(() => {
  // User
  Route.get('users/:type?', 'UserController.index')
  Route.post('user', 'UserController.store')
  Route.get('user/:id?', 'UserController.show')
  Route.put('user/:id?', 'UserController.update')
  Route.post('logout', 'AuthController.logout')

  //Professors
  Route.get('professors', 'ProfessorController.index')

  // Mandate
  Route.get('mandates', 'MandateController.index')

  // Withdrawal
  Route.get('withdrawals/:type?', 'WithdrawalController.index')
  Route.get('withdrawal/:id/rapporteurs', 'WithdrawalController.rapporteurs')
  Route.post('withdrawal', 'WithdrawalController.store')
  Route.get('withdrawal/:id', 'WithdrawalController.show')
  Route.put('withdrawal/:id', 'WithdrawalController.update')
  Route.get('withdrawal-situations/:type/:situation', 'WithdrawalController.listSituations')

  // Feedback
  Route.get('feedbacks', 'FeedbackController.index')
  Route.post('withdrawal/:id/feedback', 'FeedbackController.store')
  Route.get('feedback/:id', 'FeedbackController.show')

  // Document
  Route.get('withdrawal/:id/documents', 'DocumentController.index')

})
  .prefix('api')
  .middleware(['auth:jwt', 'authorization'])