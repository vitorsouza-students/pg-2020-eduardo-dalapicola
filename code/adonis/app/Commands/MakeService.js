'use strict'

const { Command } = require('@adonisjs/ace')
const Helpers = use('Helpers')

class MakeService extends Command {
  static get signature () {
    return 'make:service { name: Model Name } { --force?: Force service override }'
  }

  static get description () {
    return 'Make Service for current Model'
  }

  async handle (args, options) {
    if (!await this.pathExists(Helpers.appRoot(`app/Models/${args.name}.js`))) {
      this.error(`Model ${args.name} does not exists`)
      return
    }
    if (!await this.pathExists(Helpers.appRoot('app/Service/BaseService.js'))) {
      await this.writeFile(Helpers.appRoot('app/Service/BaseService.js'), `
'use strict'

class BaseService {

    repository (name) {
        return this.inject(\`App/Repository/\${name}Repository\`)
    }
    service (name) {
        return this.inject(\`App/Service/\${name}Service\`)
    }
    inject (name) {
        if(name.indexOf('Repository/') >= 0 || name.indexOf('Service/') >= 0) {
            if (!this[name]) {
              const m = use(name)
              this[name] = new m()
            }
            return this[name]
        }
        throw new Error('Just repositories can be injected')
    }
}

module.exports = BaseService      
`.trim())
    }
    const className = `${args.name}Service`
    const serName = `app/Service/${className}.js`
    if (options.force || !await this.pathExists(Helpers.appRoot(serName))) {
      await this.writeFile(Helpers.appRoot(serName), `
'use strict'

const BaseService = require("./BaseService")

class ${className} extends BaseService {

}

module.exports = ${className}     
`.trim())
      console.log(this.chalk.green('✔ create') + `  ${serName}`)
    } else {
      this.error(`${className} already exists`)
      return
    }

  }
}

module.exports = MakeService
