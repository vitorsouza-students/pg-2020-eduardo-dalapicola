'use strict'

const { Command } = require('@adonisjs/ace')
const Helpers = use('Helpers')

class MakeRepository extends Command {
  static get signature () {
    return 'make:repository { name: Model Name } { --force?: Force repository override }'
  }

  static get description () {
    return 'Make Repository for current Model'
  }

  async handle (args, options) {
    if (!await this.pathExists(Helpers.appRoot(`app/Models/${args.name}.js`))) {
      this.error(`Model ${args.name} does not exists`)
      return
    }
    if (!await this.pathExists(Helpers.appRoot('app/Repository/BaseRepository.js'))) {
      await this.writeFile(Helpers.appRoot('app/Repository/BaseRepository.js'), `
'use strict'

class BaseRepository {

    model (name) {
        return this.inject(\`App/Models/\${name}\`)
    }
    repository (name) {
        return this.inject(\`App/Repository/\${name}Repository\`)
    }
    inject (name) {
        if(name.indexOf('Models/') >= 0 || name.indexOf('Repository/') >= 0) {
            if (!this[name]) {
              const m = use(name)
              this[name] = name.indexOf('Models/') >= 0 ? m : new m()
            }
            return this[name]
        }
        throw new Error('Just models and repositories can be injected')
    }
}

module.exports = BaseRepository      
`.trim())
    }
    const className = `${args.name}Repository`
    const repName = `app/Repository/${className}.js`
    if (options.force || !await this.pathExists(Helpers.appRoot(repName))) {
      await this.writeFile(Helpers.appRoot(repName), `
'use strict'

const BaseRepository = require("./BaseRepository")

class ${className} extends BaseRepository {

}

module.exports = ${className}     
`.trim())
console.log(this.chalk.green('✔ create') + `  ${repName}`)
    } else {
      this.error(`${className} already exists`)
      return
    }

  }
}

module.exports = MakeRepository
