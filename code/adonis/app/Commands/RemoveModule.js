'use strict'

const { Command } = require('@adonisjs/ace')
const Helpers = use('Helpers')
const fs = require('fs')
const path = require('path')

class RemoveModule extends Command {
  static get signature () {
    return 'remove:module {name: Name of module}'
  }

  static get description () {
    return 'Remove model created by make:module command'
  }

  async remove (module, name) {

    let baseName = ''
    let filename = ''

    if (module === 'database/migrations') {
      const simpleName = fs.readdirSync(Helpers.appRoot(module))
        .find(fn => fn.indexOf(name.toLowerCase()) >= 0)
      baseName = path.join(module, simpleName)
      filename = Helpers.appRoot(baseName)

    } else {
      baseName = `${module}/${name}.js`
      filename = Helpers.appRoot(baseName)
    }
    if (await this.pathExists(filename) && filename.endsWith('.js')) {
      await this.removeFile(filename)
      console.log(this.chalk.green('✔ removed') + `  ${baseName}`)
    }
  }

  async handle (args, options) {
    if (await this.confirm(`Are you sure you want to delete ${args.name} module?`)) {
      await this.remove('app/Models', args.name)
      await this.remove('app/Controllers/Http', `${args.name}Controller`)
      await this.remove('app/Repository', `${args.name}Repository`)
      await this.remove('app/Service', `${args.name}Service`)
      await this.remove('database/seeds', `${args.name}Seeder`)
      await this.remove('database/migrations', args.name)
    }



  }
}

module.exports = RemoveModule
