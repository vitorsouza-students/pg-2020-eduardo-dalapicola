'use strict'

const { Command } = require('@adonisjs/ace')
const { sanitizor } = use('Validator')
const Helpers = use('Helpers')

String.prototype.toKebabCase = function () {
    return this
      .replace(/([a-z])([A-Z])/g, "$1-$2")
      .replace(/\s+/g, '-')
      .toLowerCase();
}

class MakeControllerApiOnly extends Command {
  static get signature () {
    return 'make:controller:api:only {name: Controller Name} {--force?: force controller override}'
  }

  static get description () {
    return 'Create api only controller with service injection'
  }

  async handle (args, options) {
    const className = `${args.name}Controller`
    if (options.force || !await this.pathExists(Helpers.appRoot(`app/Controllers/Http/${className}.js`))) {
      const kebabName = args.name.toKebabCase()
      const kebabNamePlural = sanitizor.plural(args.name).toKebabCase()
      await this.writeFile(Helpers.appRoot(`app/Controllers/Http/${className}.js`), `
'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with ${kebabNamePlural}
 */
class ${className} {


    static get inject () {
        return ['App/Service/${args.name}Service']
    }

    constructor(${args.name}Service) {
        this.${args.name}Service  = ${args.name}Service 
    }


  /**
   * Show a list of all ${kebabNamePlural}.
   * GET ${kebabNamePlural}
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {
  }

  /**
   * Create/save a new ${kebabName}.
   * POST ${kebabName}
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single ${kebabName}.
   * GET ${kebabName}/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
  }

  /**
   * Update ${kebabName} details.
   * PUT or PATCH ${kebabName}/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a ${kebabName} with id.
   * DELETE ${kebabName}/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ${className} 
`.trim())
    console.log(this.chalk.green('✔ create') + `  app/Controllers/Http/${className}.js`)
    } else {
      this.error(`${className} already exists`)
      return
    }
  }
}

module.exports = MakeControllerApiOnly
