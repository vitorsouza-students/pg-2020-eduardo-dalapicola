'use strict'

const ace = require('@adonisjs/ace')
const Database = use('Database')
const Helpers = use('Helpers')
const fs = require('fs')
const fse = require('fs-extra')
const Env = use('Env')
const relativePath = Env.get('STORAGE_PATH') || ''
const storagePath = Helpers.publicPath(relativePath)

class DatabaseFresh extends ace.Command {
  static get signature () {
    return 'database:fresh {--seed? : Run seed also}'
  }

  static get description () {
    return 'Fresh database and run migration'
  }

  async handle (args, options) {
    let dbconfig = Database.Config._config.database
    console.log(dbconfig[dbconfig.connection])
    if (await this
      .confirm('Are you sure you want to fresh the database?')
    ) {
      if (dbconfig.connection === 'pg') {
        await Database.raw(`
          DO $$ DECLARE
              r RECORD;
          BEGIN
              FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
                  EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
              END LOOP;
          END $$;
        `);
      }
      Database.close()
      ace.call('migration:run', {}, options)
      if (fs.existsSync(storagePath)) {
        fs.rmdirSync(storagePath, { recursive: true })
      }
      fse.copySync(Helpers.publicPath() + '_seed', Helpers.publicPath())
    }
  }
}

module.exports = DatabaseFresh
