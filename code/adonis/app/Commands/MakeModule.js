'use strict'

const ace = require('@adonisjs/ace')

class MakeModule extends ace.Command {
  static get signature () {
    return 'make:module {name: Name of module}'
  }

  static get description () {
    return 'Create module with model, controller, migration, repository and service and seed'
  }

  async handle (args, options) {
    await ace.call('make:model', args ,{migration: true})
    await ace.call('make:controller:api:only', args)
    await ace.call('make:repository', args)
    await ace.call('make:service', args)
    await ace.call('make:seed', args)
  }
}

module.exports = MakeModule
