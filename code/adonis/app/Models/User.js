'use strict'

const ModelBase = require('./ModelBase')
const Withdrawal = use('App/Models/Withdrawal')
const Feedback = use('App/Models/Feedback')
const UserRepository = use('App/Repository/UserRepository')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
// const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
const Mandate = use('App/Models/Mandate')

class User extends ModelBase {
  static get hidden () {
    return ['password']
  }
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  professor () {
    return this.belongsTo('App/Models/Professor')
  }

  secretary () {
    return this.belongsTo('App/Models/Secretary')
  }

  isSecretary () {
    return this.secretary_id !== null
  }

  isProfessor () {
    return this.professor_id !== null
  }

  async canEmitFeedback (withdrawalId) {
    return this.isSecretary() ||
      (
        // Verifica se o professor não fez parecer para esse afastamento
        await Feedback.query()
          .where('professor_id', this.professor_id)
          .where('withdrawal_id', withdrawalId)
          .first() === null
        &&
        // Verifica se o professor não é o solicitante do afastamento
        await Withdrawal.query()
          .where('id', withdrawalId)
          .where('solicitor_professor_id', this.professor_id)
          .first() !== null
      )


  }

  async isChief () {
    return this.isProfessor() &&
      (await
        (await this
          .professor()
          .first())
          .mandates()
          .whereRaw('CURRENT_DATE between mandates.begin_date and mandates.end_date')
          .where('mandates.is_chief', true)
          .first()) !== null

  }

  async canBeRapporteur (rapporteurProfessorId, withdrawalId) {
    const userRepository = new UserRepository()
    const withdrawal = await Withdrawal.findByOrFail('id', withdrawalId)
    return this.isProfessor() &&
      rapporteurProfessorId &&
      (await userRepository.checkForRapporteurs(withdrawal.solicitor_professor_id)
        .where('users.professor_id', rapporteurProfessorId)
        .first()) !== null
  }

  async logout (auth) {
    // for currently loggedin user
    const apiToken = auth.getAuthHeader()

    await auth
      .revokeTokens([apiToken])
  }
  async logoutAll (auth) {
    await auth
      .revokeTokensForUser(this)
  }
}

module.exports = User
