'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const ModelBase = require('./ModelBase')

class Mandate extends ModelBase {
}

module.exports = Mandate
