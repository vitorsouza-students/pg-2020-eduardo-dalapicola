'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ModelBase extends Model {
    static boot() {
        super.boot()

        this.addTrait('@provider:Lucid/When')
    }
}

module.exports = ModelBase
