'use strict'

const ModelBase = require('./ModelBase')

class Withdrawal extends ModelBase {

  feedbacks () {
    return this.hasMany('App/Models/Feedback')
  }

  documents () {
    return this.hasMany('App/Models/Document')
  }

}

module.exports = Withdrawal
