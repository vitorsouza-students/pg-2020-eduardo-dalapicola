'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Professor extends Model {

    mandates () {
        return this.hasMany('App/Models/Mandate')
    }
}

module.exports = Professor
