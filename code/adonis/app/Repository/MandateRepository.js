'use strict'

const BaseRepository = require("./BaseRepository")
class MandateRepository extends BaseRepository {


    async all (professorId = null) {
        return await this.model()
            .query()
            .select('id', 'begin_date', 'end_date', 'is_chief')
            .when(professorId, (query) => {
                return query
                    .where('professor_id', professorId)
            })
            .fetch()
    }

    async active (professorId) {
        return await this.model()
            .query()
            .select('begin_date', 'end_date', 'is_chief')
            .where('professor_id', professorId)
            .whereRaw('CURRENT_DATE between begin_date AND end_date')
            .first()
    }

    async removeExceptIds (ids, professorId, trx) {
        return await this.model().query({ client: trx })
            .whereNotIn('id', ids)
            .where('professor_id', professorId)
            .delete()
    }

    async findById (id) {
        return await this.model().findBy('id', id)
    }

    async updateOrCreateMany ({ mandates, professorId }, trx) {
        for (const mandate of mandates) {
            if (!Boolean(mandate.id)) {
                const relativeData = {
                    ...mandate,
                    professor_id: professorId
                }
                await this.model().create(relativeData, trx)
            } else {
                const model = await this.findById(mandate.id)
                model.merge(mandate)
                console.log(model)

                await model.save(trx)
            }
        }
    }

    async isChiefByDate (id, { begin_date, end_date }) {
        return await this.model().query()
            .where('is_chief', true)
            .when(id, (q) => {
                return q
                    .where('id', '<>', id)
            })
            .where((q) => {
                return q
                    .whereBetween('begin_date', [begin_date, end_date])
                    .orWhereBetween('end_date', [begin_date, end_date])
            })
            .first() !== null
    }

    async checkChiefMandate (mandates) {
        for (const { id, begin_date, end_date, is_chief } of mandates) {
            if (is_chief && await this.isChiefByDate(id, { begin_date, end_date })) {
                return true
            }
        }
        return false
    }

}

module.exports = MandateRepository