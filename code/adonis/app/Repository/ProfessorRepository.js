'use strict'

const BaseRepository = require("./BaseRepository")

class ProfessorRepository extends BaseRepository {

    async all () {
        return await this.model().query()
            .join('users', 'users.professor_id', '=', 'professors.id')
            .select('professors.id', 'users.username')
            .orderBy('users.username')
            .fetch()
    }

}

module.exports = ProfessorRepository