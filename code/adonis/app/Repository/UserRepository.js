'use strict'

const BaseRepository = require("./BaseRepository")
const DB = use('Database')
class UserRepository extends BaseRepository {
    async create (data, trx) {
        return await this.model().create(data, trx)
    }
    async findByEmail (email) {
        return await this.model().findByOrFail('email', email)
    }

    async findById (id) {
        return await this.model().findByOrFail('id', id)
    }

    async findWithActiveMandate (user) {
        let activeMandate = undefined
        let isChief = undefined
        if (user.isProfessor()) {
            activeMandate = await this.repository('Mandate').active(user.professor_id)
            isChief = await user.isChief()
        }
        return { ...(user.toJSON()), is_chief: isChief, active_mandate: activeMandate }
    }

    async findWithMandatesAndRelatives (user) {
        let mandates = undefined
        let relatives = undefined
        let isChief = undefined
        if (user.isProfessor()) {
            mandates = await this.repository('Mandate').all(user.professor_id)
            relatives = await this.repository('Relative').all(user.professor_id)
            isChief = await user.isChief()
        }

        return { ...(user.toJSON()), is_chief: isChief, relatives, mandates }
    }

    async all (type = null) {
        return await this.model()
            .query()
            .select(DB.raw(`
                  id
                , email
                , username
                , CASE WHEN users.professor_id is not null
                        THEN 'Professor'
                        ELSE 'Secretário'
                  END as type
            `))
            .when(type, (query) => {
                if (type === 'professor') {
                    query.whereRaw('professor_id is not null')
                } else {
                    query.whereRaw('secretary_id is not null')
                }
            })
            .orderBy('id', 'desc')
            .fetch()
    }

    async listRapporteurs (professorId) {
        return await this.checkForRapporteurs(professorId)
            .orderBy('users.username')
            .fetch()
    }

    checkForRapporteurs (professorId) {
        return this.model().query()
            .select('users.professor_id as id', 'users.username')
            .leftJoin('mandates', 'mandates.professor_id', '=', 'users.professor_id')
            // Check for not list you as professor
            .where('users.professor_id', '<>', professorId)
            // Check if is professor
            .whereRaw('users.professor_id is not null')
            // Check if is chief current mandate
            .whereRaw(`(
                mandates.id is null OR
                (
                    not mandates.is_chief AND                   
                    CURRENT_DATE between mandates.begin_date and mandates.end_date
                )
            )`)
            // Check if is relative
            .whereRaw(`
                not exists (
                    select 1 from relatives
                    where
                        (
                            first_professor_id = ${professorId} AND
                            second_professor_id = users.professor_id
                        ) OR
                        (
                            first_professor_id = users.professor_id AND
                            second_professor_id = ${professorId}
                        )
                        
                )
            `)
    }
}

module.exports = UserRepository