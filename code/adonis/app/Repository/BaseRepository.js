'use strict'

class BaseRepository {

    model (name = null) {
        const defaultModel = this.constructor.name.replace('Repository', '')
        return this.inject(`App/Models/${name || defaultModel}`)
    }
    repository (name) {
        return this.inject(`App/Repository/${name}Repository`)
    }
    inject (name) {
        if (name.indexOf('Models/') >= 0 || name.indexOf('Repository/') >= 0) {
            if (!this[name]) {
                const m = use(name)
                this[name] = name.indexOf('Models/') >= 0 ? m : new m()
            }
            return this[name]
        }
        throw new Error('Just models and repositories can be injected')
    }
}

module.exports = BaseRepository