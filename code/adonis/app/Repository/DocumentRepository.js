'use strict'

const BaseRepository = require("./BaseRepository")
const File = use('App/Helpers/File')
const DB = use('Database')
class DocumentRepository extends BaseRepository {

  async findById (id) {
    return await this.model().findBy('id', id)
  }

  async createMany ({ documents, withdrawalId }, trx) {
    for (const document of documents) {
      const documentData = {
        ...document,
        withdrawal_id: withdrawalId
      }
      await this.model().create(documentData, trx)
    }
  }


}

module.exports = DocumentRepository