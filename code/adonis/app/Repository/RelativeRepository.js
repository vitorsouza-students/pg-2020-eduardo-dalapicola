'use strict'

const BaseRepository = require("./BaseRepository")
// const User = use('App/Models/User')
class RelativeRepository extends BaseRepository {


    async all (professorId) {
        return await this.model().query()
            .select('relatives.id', 'relatives.type', 'relatives.second_professor_id as professor_id')
            .where('first_professor_id', professorId)
            .union(function () {
                this
                    .select('relatives.id', 'relatives.type', 'relatives.first_professor_id as professor_id')
                    .where('second_professor_id', professorId)
            })
            .distinct()
            .fetch()
    }

    async removeExceptIds (ids, professorId, trx) {
        await this.model().query({ client: trx })
            .whereNotIn('id', ids)
            .where((query) => {
                return query
                    .where('first_professor_id', professorId)
                    .orWhere('second_professor_id', professorId)
            })
            .delete()
    }

    async findById (id) {
        return await this.model().findBy('id', id)
    }

    async updateOrCreateMany ({ relatives, professorId }, trx) {
        for (const relative of relatives) {
            if (relative.professor_id !== professorId) {
                if (relative.id === undefined) {
                    const relativeData = {
                        first_professor_id: professorId,
                        second_professor_id: relative.professor_id,
                        type: relative.type
                    }
                    await this.model().create(relativeData, trx)
                } else {
                    const model = await this.findById(relative.id)
                    if (model.first_professor_id === professorId) {
                        model.second_professor_id = relative.professor_id
                    } else {
                        model.first_professor_id = relative.professor_id
                    }
                    model.type = relative.type

                    await model.save(trx)
                }
            }

        }
    }
}

module.exports = RelativeRepository