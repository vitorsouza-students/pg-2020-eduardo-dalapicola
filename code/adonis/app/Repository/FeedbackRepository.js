'use strict'

const BaseRepository = require("./BaseRepository")
const DB = use('Database')
class FeedbackRepository extends BaseRepository {

  async all (professorId) {
    return await this.model().query()
      .select('id', 'judgment', 'withdrawal_id', 'created_at')
      .where('professor_id', professorId)
      .orderBy('id', 'desc')
      .fetch()
  }

  async create (fields, withdrawalId) {
    const withDrawal = await this.repository('Withdrawal').findById(withdrawalId)
    return await withDrawal.feedbacks().create(fields)
  }

  async findById (id) {
    return await this.model().findBy('id', id)
  }

  async checkRapporteurFeedback (withdrawal) {
    return withdrawal.type === 'Nacional' ||
      (
        withdrawal.rapporteur_professor_id !== null &&
        await this.model().query()
          .where('withdrawal_id', withdrawal.id)
          .where('professor_id', withdrawal.rapporteur_professor_id)
          .where('judgment', 'Favorável')
          .first() !== null
      )
  }

}

module.exports = FeedbackRepository