'use strict'

const BaseRepository = require("./BaseRepository")
const DB = use('Database')
class WithdrawalRepository extends BaseRepository {

  async all (user, params) {
    return this.model().query()
      .join('users', 'users.professor_id', '=', 'withdrawals.solicitor_professor_id')
      .select(DB.raw(`
          withdrawals.id
        , withdrawals.created_at
        , users.username as solicitor_name
        , users.id as solicitor_id
        , situation
        , ( users.id <> ${user.id} AND
            not exists
            (
              select 1 
                from feedbacks
              where
                professor_id = ${user.professor_id} and
                withdrawal_id = withdrawals.id
            )
          ) as emit_feedback        
      `))
      .when(params.type && user.isProfessor(), query => {
        if (params.type === 'rapporteur') {
          return query
            .where('withdrawals.rapporteur_professor_id', user.professor_id)
        } else {
          const op = params.type === 'mine' ? '=' : '<>'
          return query
            .where('users.id', op, user.id)
        }
      })
      .orderBy('id', 'DESC')
      .fetch()
  }

  async create (data, trx) {
    if (data.type === 'Nacional') {
      data.rapporteur_professor_id = null
    }
    return await this.model().create(data, trx)
  }

  async findById (id) {
    return await this.model().findByOrFail('id', id)
  }

  async update (fields, id, trx) {
    const withdrawal = await this.findById(id)
    withdrawal.merge(fields)

    if (withdrawal.type === 'Nacional') {
      withdrawal.rapporteur_professor_id = null
    }

    await withdrawal.save(trx)
    await withdrawal.reload()
    return withdrawal
  }

  async documents (id) {
    return await (await this.findById(id))
      .documents()
      .select('id', 'title', 'url')
      .orderBy('id', 'desc')
      .fetch()
  }
}

module.exports = WithdrawalRepository