'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Feedback = use('App/Models/Feedback')
const WithdrawalService = use('App/Service/WithdrawalService')
class Authorization {
  static get handler () {
    return {
      "GET users/:type": ({ auth }) => {
        if (auth.user.isProfessor()) {
          return 'Você não tem permissão para visualizar usuários'
        }
      },
      "GET user/:id?": ({ auth, params }) => {
        if (auth.user.isProfessor() && params.id && auth.user.id !== Number(params.id)) {
          return 'Você não tem permissão para visualizar esse usuário'
        }

      },
      "POST user": ({ auth, request }) => {
        if (auth.user.isProfessor() && request.type !== 'professor') {
          return 'Você não tem permissões para criar um usuário administrativo'
        }

      },
      "PUT user/:id?": ({ auth, params }) => {
        if (auth.user.isProfessor() && params.id && auth.user.id !== Number(params.id)) {
          return 'Você não tem permissão para atualizar esse usuário'
        }
      },
      "GET professors": async ({ auth }) => {
        if (auth.user.isProfessor() && !await auth.user.isChief()) {
          return 'Você não tem permissão para visualizar Professores'
        }
      },
      "POST withdrawal": ({ auth }) => {
        if (auth.user.isSecretary()) {
          return 'Você não tem permissão para criar Afastamentos'
        }
      },
      "PUT withdrawal/:id": async ({ auth, params, request }) => {
        const rapporteurProfessorId = request.input('rapporteur_professor_id')
        const toSituation = request.input('situation')
        const withdrawalService = new WithdrawalService()
        const msg = await withdrawalService.checkValidSituation(params.id, toSituation)
        if (typeof (msg) === 'string') {
          return msg
        }
        if (rapporteurProfessorId) {
          if (!await auth.user.canBeRapporteur(rapporteurProfessorId, params.id)) {
            return 'Esse usuário não pode ser relator deste afastamento'
          }
        }
      },
      "GET feedbacks": ({ auth }) => {
        if (auth.user.isSecretary()) {
          return 'Você não tem permissão para visualizar Pareceres.'
        }
      },
      "POST withdrawal/:id/feedback": async ({ auth, params }) => {
        if (auth.user.isSecretary() || await auth.user.canEmitFeedback(params.id)) {
          return 'Você não tem permissão para criar Parecer.'
        }
      },
      "GET feedback/:id": async ({ auth, params }) => {
        const feedback = await Feedback.findByOrFail('id', params.id)
        if (auth.user.isSecretary() || auth.user.professor_id !== feedback.professor_id) {
          return "Você não tem permissão de recuperar esse Parecer"
        }
      }
    }
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle (context, next) {
    const { request, response } = context
    const key = Object.keys(Authorization.handler).find((item) => {
      const [method, url] = item.split(' ')
      return method === request.method() && request.match(['/api/' + url])
    })
    const f = Authorization.handler[key]
    if (f && typeof (f) === 'function') {
      const isAsync = f.constructor.name === "AsyncFunction";
      const message = isAsync ? await f(context) : f(context)
      if (message) {
        return response.forbidden({
          message
        })
      }
    }


    // call next to advance the request
    await next()
  }
}

module.exports = Authorization
