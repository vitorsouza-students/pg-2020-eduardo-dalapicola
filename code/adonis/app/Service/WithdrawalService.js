'use strict'

const BaseService = require("./BaseService")
const File = use('App/Helpers/File')

const internationalSituationTransitions = {
  'Iniciado': ['Iniciado', 'Aprovado-DI', 'Reprovado', 'Cancelado'],
  'Aprovado-DI': ['Aprovado-DI', 'Aprovado-CT', 'Aprovado-PRPPG', 'Reprovado', 'Cancelado'],
  'Aprovado-CT': ['Aprovado-CT', 'Aprovado-PRPPG', 'Reprovado', 'Cancelado'],
  'Aprovado-PRPPG': ['Aprovado-PRPPG', 'Aprovado-CT', 'Arquivado', 'Reprovado', 'Cancelado'],
  'Arquivado': ['Arquivado'],
  'Cancelado': ['Cancelado'],
  'Reprovado': ['Reprovado']
}

const nationalSituationTransitions = {
  'Iniciado': ['Iniciado', 'Aprovado-DI', 'Reprovado', 'Cancelado'],
  'Aprovado-DI': ['Aprovado-DI', 'Arquivado', 'Reprovado', 'Cancelado'],
  'Arquivado': ['Arquivado'],
  'Cancelado': ['Cancelado'],
  'Reprovado': ['Reprovado']
}
class WithdrawalService extends BaseService {

  async create ({ withdrawalFields, documents, urls }) {
    await File.transaction(urls, async (trx) => {
      const withdrawal = await this.repository('Withdrawal').create(withdrawalFields, trx)
      await this.service('Document').createMany({ documents, withdrawalId: withdrawal.id }, trx)
    })
  }

  async update ({ withdrawalFields, documents, urls }, id) {
    await File.transaction(urls, async (trx) => {
      const withdrawal = await this.repository('Withdrawal').update(withdrawalFields, id, trx)
      await this.service('Document').createMany({ documents, withdrawalId: withdrawal.id }, trx)
    })
  }

  async listRapporteurs (id) {
    const withdrawal = await this.repository('Withdrawal').findById(id)
    return await this.repository('User').listRapporteurs(withdrawal.solicitor_professor_id)
  }

  async checkValidSituation (withdrawalId, toSituation) {

    if (toSituation === undefined) return true

    const withdrawal = await this.repository('Withdrawal').findById(withdrawalId)
    const fromSituation = withdrawal.situation


    const check = withdrawal.type === 'Internacional'
      ? internationalSituationTransitions[fromSituation].indexOf(toSituation) >= 0
      : nationalSituationTransitions[fromSituation].indexOf(toSituation) >= 0

    if (!check) return 'Você não pode atualizar a situação para ' + toSituation

    if (toSituation.indexOf('Aprovado') >= 0 || toSituation === 'Arquivado') {
      const checkRap = await this.repository('Feedback').checkRapporteurFeedback(withdrawal)
      if (!checkRap) return 'Você ainda não pode Aprovar o afastamento, o relator ainda não enviou o parecer ou o parecer foi negativo.'
    }

    return true

  }

  listSituations (type, situation) {
    const list = type === 'Internacional'
      ? internationalSituationTransitions[situation]
      : nationalSituationTransitions[situation]
    return list.filter((item) => item !== 'Cancelado')

  }
}

module.exports = WithdrawalService