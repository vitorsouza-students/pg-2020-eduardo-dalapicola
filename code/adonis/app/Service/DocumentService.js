'use strict'

const BaseService = require("./BaseService")

class DocumentService extends BaseService {

  async createMany ({ documents, withdrawalId }, trx) {
    await this.repository('Document').createMany({ documents, withdrawalId }, trx)
  }

}

module.exports = DocumentService