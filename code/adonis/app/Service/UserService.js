'use strict'

const Professor = use("App/Models/Professor")
const Secretary = use("App/Models/Secretary")
const BaseService = require("./BaseService")
const DB = use('Database')


class UserService extends BaseService {

    async login ({ auth, email, password }) {
        if (await auth.attempt(email, password)) {
            let user = await this.repository('User').findByEmail(email)
            const accessToken = await auth.generate(user)


            return { ...accessToken, user: await this.repository('User').findWithActiveMandate(user) }
        }
        return false
    }

    async create ({ userData, relatives, mandates }, type) {
        await DB.transaction(async (trx) => {
            const user = await this.repository('User').create(userData, trx)
            if (type === 'professor') {
                const professor = await Professor.create({}, trx)
                await user.professor().associate(professor, trx)
                await this.service('Relative').update({ relatives, professorId: professor.id }, trx)
                await this.service('Mandate').update({ mandates, professorId: professor.id }, trx)
            } else {
                const secretary = await Secretary.create({}, trx)
                await user.secretary().associate(secretary, trx)
            }
        })
    }


    async update ({ userData, relatives, mandates }, id) {
        const user = await this.repository('User').findById(id)
        user.merge(userData)
        await DB.transaction(async (trx) => {
            if (user.isProfessor()) {
                await this.service('Relative').update({ relatives, professorId: user.professor_id }, trx)
                await this.service('Mandate').update({ mandates, professorId: user.professor_id }, trx)
            }
            await user.save(trx)
            return user
        })
        await user.reload()
        return user

    }

}

module.exports = UserService
