'use strict'

const BaseService = require("./BaseService")

class MandateService extends BaseService {

    async update ({ mandates, professorId }, trx) {
        const ids = mandates
            .filter((item) => item.id !== undefined)
            .map((item) => item.id)
        await this.repository('Mandate').removeExceptIds(ids, professorId, trx)
        await this.repository('Mandate').updateOrCreateMany({ mandates, professorId }, trx)
    }

}

module.exports = MandateService