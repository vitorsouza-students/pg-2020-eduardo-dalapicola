'use strict'

class BaseService {

    repository (name) {
        return this.inject(`App/Repository/${name}Repository`)
    }
    service (name) {
        return this.inject(`App/Service/${name}Service`)
    }
    inject (name) {
        if (name.indexOf('Repository/') >= 0 || name.indexOf('Service/') >= 0) {
            if (!this[name]) {
                const m = use(name)
                this[name] = new m()
            }
            return this[name]
        }
        throw new Error('Just repositories can be injected')
    }
}

module.exports = BaseService