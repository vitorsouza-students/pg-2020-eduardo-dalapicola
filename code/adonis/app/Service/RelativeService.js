'use strict'

const BaseService = require("./BaseService")

class RelativeService extends BaseService {


    async update ({ relatives, professorId }, trx) {
        const ids = relatives
            .filter((item) => item.id !== undefined)
            .map((item) => item.id)
        await this.repository('Relative').removeExceptIds(ids, professorId, trx)
        await this.repository('Relative').updateOrCreateMany({ relatives, professorId }, trx)
    }
}

module.exports = RelativeService