'use strict'

const BaseService = require("./BaseService")

class FeedbackService extends BaseService {
  async create (fields, withdrawalId) {
    return await this.repository('Feedback').create(fields, withdrawalId)
  }
}

module.exports = FeedbackService