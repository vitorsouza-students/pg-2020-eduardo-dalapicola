const Helpers = use('Helpers')
const path = require('path')
const urljoin = require('url-join')
const Chance = require('chance')
const fs = require('fs')
const removeFile = Helpers.promisify(fs.unlink)
const chance = new Chance()
const Env = use('Env')
const DB = use('Database')


// Constants
const url = Env.get('APP_URL')
const relativePath = Env.get('STORAGE_PATH') || ''
const basePath = Helpers.publicPath(relativePath)

function randomFilename (filename) {
  const { dir, ext } = path.parse(filename)
  return path.join(dir, chance.string({ length: 30, pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' }) + ext)
}

function getFullPath (localUrl) {
  const baseUrl = urljoin(url, relativePath)
  return path.join(basePath, path.relative(baseUrl, localUrl))
}
function getUrl (file) {
  return urljoin(url, relativePath, file.fileName)
}

async function put (file, folder = '') {
  await file.move(basePath, {
    name: randomFilename(path.join(folder, file.clientName)),
    overwrite: true
  })

  if (!file.moved()) {
    throw file.error()
  }

  return getUrl(file)
}

async function putMany (files, folder = '') {
  await files.moveAll(basePath, {
    name: randomFilename(path.join(folder, file.clientName)),
    overwrite: true
  })

  const movedFiles = profilePics.movedList()

  if (!file.movedAll()) {

    await Promise.all(movedFiles.map((file) => {
      return removeFile(path.join(file._location, file.fileName))
    }))

    throw files.errors()
  }

  return movedFiles.map(getUrl)
}

function remove (url) {
  const fullPath = getFullPath(url)
  if (fs.existsSync(fullPath)) {
    fs.unlinkSync(fullPath)
  }
}

function removeMany (urls) {
  urls.forEach(remove);
}

async function transaction (urls, callback) {
  const trx = await DB.beginTransaction()
  try {
    await callback(trx)
    await trx.commit()
  } catch (error) {
    if (Array.isArray(urls)) {
      removeMany(urls)
    } else {
      remove(urls)
    }
    await trx.rollback()
    throw error
  }
}


module.exports = { put, putMany, remove, removeMany, transaction, getUrl }