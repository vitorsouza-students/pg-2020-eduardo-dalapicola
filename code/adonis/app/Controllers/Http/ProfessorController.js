'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with professors
 */
class ProfessorController {


    static get inject () {
        return ['App/Service/ProfessorService']
    }

    constructor(ProfessorService) {
        this.ProfessorService = ProfessorService
        this.ProfessorRepository = ProfessorService.repository('Professor')
    }


    /**
     * Show a list of all professors.
     * GET professors
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async index () {
        return await this.ProfessorRepository.all()
    }

    /**
     * Create/save a new professor.
     * POST professor
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async store ({ request, response }) {
    }

    /**
     * Display a single professor.
     * GET professor/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async show ({ params, request, response }) {
    }

    /**
     * Update professor details.
     * PUT or PATCH professor/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update ({ params, request, response }) {
    }

    /**
     * Delete a professor with id.
     * DELETE professor/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy ({ params, request, response }) {
    }
}

module.exports = ProfessorController