'use strict'

const WithdrawalRepository = require('../../Repository/WithdrawalRepository')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with documents
 */
class DocumentController {


    static get inject () {
        return [
            'App/Service/DocumentService',
            'App/Repository/WithdrawalRepository'
        ]
    }

    constructor(DocumentService, WithdrawalRepository) {
        this.DocumentService = DocumentService
        this.WithdrawalRepository = WithdrawalRepository
    }


    /**
     * Show a list of all documents.
     * GET documents
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async index ({ params }) {
        return await this.WithdrawalRepository.documents(params.id)
    }

    /**
     * Create/save a new document.
     * POST document
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async store ({ request, response }) {
    }

    /**
     * Display a single document.
     * GET document/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async show ({ params, request, response }) {
    }

    /**
     * Update document details.
     * PUT or PATCH document/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update ({ params, request, response }) {
    }

    /**
     * Delete a document with id.
     * DELETE document/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy ({ params, request, response }) {
    }
}

module.exports = DocumentController