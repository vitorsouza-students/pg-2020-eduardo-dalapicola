'use strict'

const File = use('App/Helpers/File')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with withdrawals
 */
class WithdrawalController {


    static get inject () {
        return ['App/Service/WithdrawalService']
    }

    constructor(WithdrawalService) {
        this.WithdrawalService = WithdrawalService
        this.WithdrawalRepository = WithdrawalService.repository('Withdrawal')
    }


    /**
     * Show a list of all withdrawals.
     * GET withdrawals
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async index ({ auth, params }) {
        return await this.WithdrawalRepository.all(auth.user, params)
    }

    async rapporteurs ({ params }) {
        return await this.WithdrawalService.listRapporteurs(params.id)
    }

    async #parseDocuments (request) {
        const getFile = (pos) => {
            return request.file(`documents[${pos}]['file']`, {
                types: ['image', 'application'],
                size: '5mb',
                extnames: ['png', 'jpg', 'jpeg', 'pdf']
            })
        }
        const urls = []
        const documents = (request.input('documents') || [])
        let i = 0
        for (const document of documents) {
            const file = getFile(i++)
            if (file) {
                document.url = await File.put(file, 'documents')
                urls.push(document.url)
            }
        }
        return { documents, urls }
    }

    /**
     * Create/save a new withdrawal.
     * POST withdrawal
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async store ({ request, auth }) {
        const withdrawalFields = request.only([
            'begin_date'
            , 'end_date'
            , 'type'
            , 'onus'
            , 'event_name'
            , 'event_begin_date'
            , 'event_end_date'
            , 'reason'
        ])
        if (auth.user.isProfessor()) {

            withdrawalFields.solicitor_professor_id = auth.user.professor_id
        }

        const { documents, urls } = await this.#parseDocuments(request)

        await this.WithdrawalService.create({ withdrawalFields, documents, urls })

        return {
            message: "Afastamento criado com sucesso"
        }
    }

    /**
     * Display a single withdrawal.
     * GET withdrawal/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async show ({ params }) {
        return (await this.WithdrawalRepository.findById(params.id)).toJSON()
    }

    /**
     * Update withdrawal details.
     * PUT or PATCH withdrawal/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update ({ auth, params, request }) {
        const withdrawalFields = {}
        const situation = request.input('situation')
        if (auth.user.isSecretary()) {
            withdrawalFields.situation = situation
        }
        if (auth.user.isProfessor()) {
            if (await auth.user.isChief()) {
                withdrawalFields.rapporteur_professor_id = request.input('rapporteur_professor_id')
            }
            if (typeof (situation) === 'string') {
                withdrawalFields.situation = 'Cancelado'
            }
        }

        const { documents, urls } = await this.#parseDocuments(request)

        const data = { withdrawalFields, documents, urls }
        await this.WithdrawalService.update(data, params.id)

        return {
            message: "Afastamento atualizado com sucesso"
        }

    }

    async listSituations ({ params }) {
        return this.WithdrawalService.listSituations(params.type, params.situation)
    }

    /**
     * Delete a withdrawal with id.
     * DELETE withdrawal/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy ({ params, request, response }) {
    }
}

module.exports = WithdrawalController