'use strict'

class UserController {
    static get inject () {
        return ['App/Service/UserService', 'App/Repository/MandateRepository']
    }

    constructor(UserService, MandateRepository) {
        this.UserService = UserService
        this.UserRepository = UserService.repository('User')
        this.MandateRepository = MandateRepository
    }

    async index ({ params }) {
        return await this.UserRepository.all(params.type)
    }

    async #getUserData (auth, user) {
        if (auth.check() && auth.user.isSecretary()) {
            return await this.UserRepository.findWithMandatesAndRelatives(user)
        } else {
            return await this.UserRepository.findWithActiveMandate(user)
        }
    }

    async show ({ params, auth }) {
        const id = Number(params.id) || auth.user.id
        const user = await this.UserRepository.findById(id)
        return await this.#getUserData(auth, user)
    }

    async #_store ({ request, response, auth }, type) {
        const userData = request.only(['username', 'password', 'email'])

        let relatives = []
        let mandates = []

        if (auth.user.isSecretary()) {
            relatives = request.input('relatives') || []
            mandates = request.input('mandates') || []
        }

        if (await this.MandateRepository.checkChiefMandate(mandates)) {
            return response.unprocessableEntity({ message: 'já existe Chefe de departamento para esse mandato' })
        }

        await this.UserService.create({ userData, relatives, mandates }, type)
    }

    async store (funcParams) {
        const request = funcParams.request
        await this.#_store(funcParams, request.input('type'))

        return {
            message: "Usuário criado com sucesso"
        }
    }

    async storeGuest (funcParams) {

        await this.#_store(funcParams, 'professor')

        return {
            message: "Você criou sua conta com sucesso!"
        }
    }

    async update ({ request, params, auth, response }) {
        const userData = request.only(['username', 'password', 'email'])
        const id = Number(params.id) || auth.user.id

        let relatives = []
        let mandates = []

        if (auth.user.isSecretary()) {
            relatives = request.input('relatives') || []
            mandates = request.input('mandates') || []
        }


        if (await this.MandateRepository.checkChiefMandate(mandates)) {
            return response.unprocessableEntity({ message: 'Já existe chefe de departamento para esse mandato' })
        }

        const user = await this.UserService.update({ userData, relatives, mandates }, id)
        return await this.#getUserData(auth, user)
    }
}

module.exports = UserController
