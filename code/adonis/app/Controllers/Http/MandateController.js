'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with mandates
 */
class MandateController {


    static get inject () {
        return ['App/Service/MandateService']
    }

    constructor(MandateService) {
        this.MandateService = MandateService
        this.MandateRepository = MandateService.repository('Mandate') 
    }


  /**
   * Show a list of all mandates.
   * GET mandates
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ params }) {
      return await this.MandateRepository.all(params.id)
  }

  /**
   * Create/save a new mandate.
   * POST mandate
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single mandate.
   * GET mandate/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
  }

  /**
   * Update mandate details.
   * PUT or PATCH mandate/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a mandate with id.
   * DELETE mandate/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = MandateController