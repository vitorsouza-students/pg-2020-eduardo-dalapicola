'use strict'

class AuthController {


    static get inject () {
        return ['App/Service/UserService']
    }

    constructor(UserService) {
        this.UserService = UserService 
    }

    async login ({ auth, request }) {
        const data = request.only(['email', 'password'])
        data.auth = auth
        const res = await this.UserService.login(data)
        if(res) {
            res.message = 'Olá, seja bem-vindo à SCAP'
            return res
        }
    }

    async logout ({ auth }) {            
        await auth.user.logout(auth)
        return {
            message: "Obrigado por nos visitar, até mais!"
        }
        
    }

}

module.exports = AuthController
