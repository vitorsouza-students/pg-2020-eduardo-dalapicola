'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with feedbacks
 */
class FeedbackController {


    static get inject () {
        return ['App/Service/FeedbackService']
    }

    constructor(FeedbackService) {
        this.FeedbackService = FeedbackService
        this.FeedbackRepository = FeedbackService.repository('Feedback')
    }


    /**
     * Show a list of all feedbacks.
     * GET feedbacks
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async index ({ auth }) {
        const id = auth.user.professor_id
        return await this.FeedbackRepository.all(id)
    }

    /**
     * Create/save a new feedback.
     * POST feedback
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async store ({ request, params, auth }) {
        const feedbackFields = request.only(['reason', 'judgment'])
        feedbackFields['professor_id'] = auth.user.professor_id
        return await this.FeedbackService.create(feedbackFields, params.id)
    }

    /**
     * Display a single feedback.
     * GET feedback/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async show ({ params }) {
        return await this.FeedbackRepository.findById(params.id)
    }

    /**
     * Update feedback details.
     * PUT or PATCH feedback/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update ({ params, request, response }) {
    }

    /**
     * Delete a feedback with id.
     * DELETE feedback/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy ({ params, request, response }) {
    }
}

module.exports = FeedbackController