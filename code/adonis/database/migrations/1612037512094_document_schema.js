'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DocumentSchema extends Schema {
  up () {
    this.create('documents', (table) => {
      table.increments()

      table.string('title', 200).notNullable()
      table.string('url', 200).notNullable()

      table.integer('withdrawal_id').unsigned().notNullable();
      table.foreign('withdrawal_id').references('id').inTable('withdrawals');

      table.timestamps()
    })
  }

  down () {
    this.drop('documents')
  }
}

module.exports = DocumentSchema
