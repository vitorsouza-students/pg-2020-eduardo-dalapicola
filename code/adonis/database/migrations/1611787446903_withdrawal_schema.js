'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WithdrawalSchema extends Schema {
  up () {
    this.create('withdrawals', (table) => {
      table.increments()

      table.date('begin_date').notNullable()
      table.date('end_date').notNullable()
      table.enum('type', ['Nacional', 'Internacional']).notNullable()
      table.enum('onus', ['Total', 'Parcial', 'Inesistente']).notNullable()
      table.enum('situation', ['Iniciado', 'Aprovado-DI', 'Aprovado-CT', 'Aprovado-PRPPG', 'Arquivado', 'Cancelado', 'Reprovado']).notNullable().default('Iniciado')
      table.string('reason', 500).notNullable()

      table.string('event_name', 200).notNullable()
      table.date('event_begin_date').notNullable()
      table.date('event_end_date').notNullable()

      table.integer('rapporteur_professor_id').unsigned();
      table.foreign('rapporteur_professor_id').references('id').inTable('professors');

      table.integer('solicitor_professor_id').unsigned().notNullable();
      table.foreign('solicitor_professor_id').references('id').inTable('professors');

      table.timestamps()
    })
  }

  down () {
    this.drop('withdrawals')
  }
}

module.exports = WithdrawalSchema
