'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FeedbackSchema extends Schema {
  up () {
    this.create('feedbacks', (table) => {
      table.increments()

      table.enum('judgment', ['Favorável', 'Desfavorável']).notNullable()
      table.string('reason').notNullable()

      table.integer('withdrawal_id').unsigned().notNullable();
      table.foreign('withdrawal_id').references('id').inTable('withdrawals');

      table.integer('professor_id').unsigned().notNullable();
      table.foreign('professor_id').references('id').inTable('professors');

      table.unique(['withdrawal_id', 'professor_id'])

      table.timestamps()
    })
  }

  down () {
    this.drop('feedbacks')
  }
}

module.exports = FeedbackSchema
