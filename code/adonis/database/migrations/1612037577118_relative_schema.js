'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RelativeSchema extends Schema {
  up () {
    this.create('relatives', (table) => {
      table.increments()

      table.enum('type', ['Sanguíneo', 'Matrimonial']).notNullable()

      table.integer('first_professor_id').unsigned().notNullable();
      table.foreign('first_professor_id').references('id').inTable('professors');

      table.integer('second_professor_id').unsigned().notNullable();
      table.foreign('second_professor_id').references('id').inTable('professors');

      table.unique(['first_professor_id', 'second_professor_id'])

      table.timestamps()
    })
  }

  down () {
    this.drop('relatives')
  }
}

module.exports = RelativeSchema
