'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SecretarySchema extends Schema {
  up () {
    this.create('secretaries', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('secretaries')
  }
}

module.exports = SecretarySchema
