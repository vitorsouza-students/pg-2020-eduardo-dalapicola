'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MandateSchema extends Schema {
  up () {
    this.create('mandates', (table) => {
      table.increments()

      table.boolean('is_chief').notNullable().default('false')

      table.date('begin_date').notNullable()
      table.date('end_date').notNullable()

      table.integer('professor_id').unsigned().notNullable();
      table.foreign('professor_id').references('id').inTable('professors');

      table.timestamps()
    })
  }

  down () {
    this.drop('mandates')
  }
}

module.exports = MandateSchema
