'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Professor = use('App/Models/Professor')
const Secretary = use('App/Models/Secretary')
const moment = use('moment')
const File = use('App/Helpers/File')
const UserRepository = use('App/Repository/UserRepository')

moment.locale('pt-br')

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  const isSecretary = faker.bool()
  const isProfessor = !isSecretary
  return {
    username: faker.name({ nationality: 'en' }),
    email: faker.email(),
    password: faker.password(),
    secretary_id: isSecretary ? (await Secretary.create()).id : null,
    professor_id: isProfessor ? (await Professor.create()).id : null
  }
})

const situations = {
  "Nacional": [
    'Iniciado',
    'Aprovado-DI',
    'Arquivado',
    'Cancelado',
    'Reprovado',
  ],
  "Internacional": [
    'Iniciado',
    'Aprovado-CT',
    'Aprovado-PRPPG',
    'Arquivado',
    'Cancelado',
    'Reprovado'
  ]
}

Factory.blueprint('App/Models/Withdrawal', async (faker, i, data) => {
  const type = faker.pickone(['Nacional', 'Internacional'])
  const situation = faker.pickone(situations[type])
  const professorIds = await Professor.query().pluck('id')
  const date = moment().subtract(faker.integer({ min: 1, max: 15 }), 'days')
  const solicitor_professor_id = faker.pickone(professorIds)
  let rapporteur_professor_id = null
  if (type === 'Internacional') {
    const rapporteurIds = await (new UserRepository()).checkForRapporteurs(solicitor_professor_id).pluck('users.professor_id')
    rapporteur_professor_id = faker.pickone(rapporteurIds)
  }
  return {
    begin_date: moment().add(faker.integer({ min: 2, max: 5 }), 'days'),
    end_date: moment().add(faker.integer({ min: 6, max: 10 }), 'days'),
    type,
    onus: faker.pickone(['Total', 'Parcial', 'Inesistente']),
    situation,
    reason: faker.sentence({ words: 5 }),
    event_name: faker.word({ syllables: 3 }),
    event_begin_date: moment().add(faker.integer({ min: 2, max: 5 }), 'days'),
    event_end_date: moment().add(faker.integer({ min: 6, max: 10 }), 'days'),

    rapporteur_professor_id,
    solicitor_professor_id,
    created_at: date,
    updated_at: date
  }
})

Factory.blueprint('App/Models/Feedback', function (faker, i, data) {
  const date = moment().subtract(faker.integer({ min: 5, max: 15 }), 'days')
  return {
    judgment: data.judgment || faker.pickone(['Favorável', 'Desfavorável']),
    reason: faker.sentence({ words: 5 }),
    created_at: date,
    updated_at: date,
    ...data
  }
})

Factory.blueprint('App/Models/Document', function (faker, i, data) {
  return {
    title: faker.word(),
    url: File.getUrl({ fileName: 'documents/sample.pdf' }),
    withdrawal_id: faker.pickone(data)
  }
})