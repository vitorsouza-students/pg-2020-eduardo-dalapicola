'use strict'

/*
|--------------------------------------------------------------------------
| RelativeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class RelativeSeeder {
  async run () {
  }
}

module.exports = RelativeSeeder
