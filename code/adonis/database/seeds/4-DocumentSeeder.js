'use strict'

/*
|--------------------------------------------------------------------------
| DocumentSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Withdrawal = use('App/Models/Withdrawal')

class DocumentSeeder {
  async run () {
    const withdrawalIds = await Withdrawal.query().pluck('id')
    await Factory.model('App/Models/Document').createMany(30, withdrawalIds)
  }
}

module.exports = DocumentSeeder
