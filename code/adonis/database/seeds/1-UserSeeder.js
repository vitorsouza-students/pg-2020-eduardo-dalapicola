'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const Professor = use('App/Models/Professor')
const Secretary = use('App/Models/Secretary')
const moment = use('moment')
const path = require('path')

class UserSeeder {
  async run () {
    console.log('seeding ' + path.basename(__filename))
    const chief = await Professor.create()
    const today = moment().startOf('day')
    await chief.mandates().create({
      is_chief: true,
      begin_date: today.clone().subtract(1, 'year'),
      end_date: today.clone().add(1, 'year')
    })
    // Create Professors
    await User
      .createMany(
        [
          {
            username: 'Professor1',
            email: 'professor1@email.com',
            password: '12345678',
            professor_id: (await Professor.create()).id
          },
          {
            username: 'Professor2',
            email: 'professor2@email.com',
            password: '12345678',
            professor_id: (await Professor.create()).id
          },
          {
            username: 'Chefe de Departamento',
            email: 'chefe@email.com',
            password: '12345678',
            professor_id: chief.id
          }
        ]
      )
    // Create Secretary
    await User
      .createMany(
        [
          {
            username: 'Secretário',
            email: 'secretario@email.com',
            password: '12345678',
            secretary_id: (await Secretary.create()).id
          }
        ]
      )
    await Factory
      .model('App/Models/User')
      .createMany(20)
  }
}

module.exports = UserSeeder
