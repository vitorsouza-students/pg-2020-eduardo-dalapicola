'use strict'

/*
|--------------------------------------------------------------------------
| FeedbackSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Withdrawal = use('App/Models/Withdrawal')
const Feedback = use('App/Models/Feedback')
const Professor = use('App/Models/Professor')
const path = require('path')

class FeedbackSeeder {
  async run () {
    console.log('seeding ' + path.basename(__filename))

    const withdrawalIds = (await Withdrawal.query().pluck('id'))
      .sort(() => Math.random() - 0.5)
      .slice(1, 20)

    const randomProfessorIds = (await Professor.query().pluck('id'))
      .sort(() => Math.random() - 0.5)
      .slice(1, 5)

    const fixedProfessorIds = (await Professor.query()
      .join('users', 'users.professor_id', '=', 'professors.id')
      .whereIn('users.email', ['professor1@email.com', 'professor2@email.com', 'chefe@email.com'])
      .pluck('professors.id')
    )
    const professorIds = new Set(randomProfessorIds.concat(fixedProfessorIds))

    for (const withdrawal_id of withdrawalIds) {
      for (const professor_id of professorIds) {
        const exists = (await Feedback.query().where({ professor_id, withdrawal_id }).first()) !== null
        if (!exists) {
          await Factory.model('App/Models/Feedback').create({
            professor_id,
            withdrawal_id
          })
        }
      }
    }
  }
}

module.exports = FeedbackSeeder
