'use strict'

/*
|--------------------------------------------------------------------------
| SecretarySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class SecretarySeeder {
  async run () {
  }
}

module.exports = SecretarySeeder
