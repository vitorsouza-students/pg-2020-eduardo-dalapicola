'use strict'

/*
|--------------------------------------------------------------------------
| WithdrawalSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const path = require('path')

class WithdrawalSeeder {
  async run () {
    console.log('seeding ' + path.basename(__filename))
    const data = await Factory.model('App/Models/Withdrawal').createMany(40)
    for (const withdrawal of data) {
      if (withdrawal.situation !== 'Iniciado' && withdrawal.type === 'Internacional') {
        await Factory.model('App/Models/Feedback').create({
          withdrawal_id: withdrawal.id,
          professor_id: withdrawal.rapporteur_professor_id,
          judgment: 'Favorável'
        })
      }
    }

  }
}

module.exports = WithdrawalSeeder
